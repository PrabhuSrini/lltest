// module.exports = {
//     module:{
//         rules:[
//             {
//                 test:/\.(s*)css$/,
//                 use:['style-loader','css-loader', 'sass-loader']
//             }
//         ]
//     },
//     entry: ['./app/main.js'],
//     output: {
//         publicPath: 'public/js', // instead of publicPath: '/build/'
//         filename: 'bundle.js'
//     },
//     resolve: {
//     extensions: ['.js', '.jsx', '.scss']
//     }
//
// };


var path = require("path");

module.exports = {
    entry: "./app/main.js",
    output: {
        path: path.resolve(__dirname, "public/js"),
        filename: "bundle.js",
        publicPath: "/public/js"
    },
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    'style-loader',
                    // Translates CSS into CommonJS
                    'css-loader',
                    // Compiles Sass to CSS
                    'sass-loader',
                ],
            },
        ],
    },
};