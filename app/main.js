//console.log('Inside main.js adfasv');


import './scss/app.scss';

let students = require('../public/data/students.json');
let teachers = require('../public/data/teachers.json');
let timings = require('../public/data/timings.json');
let allClassData = require('../public/data/classes.json');
var classes = require('../public/data/classes.json');

var filteredData = [];

window.onload = function() {
    initTableHeader();
    initTableRows();
    document.getElementById("searchStudent").onkeyup = function() {
        searchStudent();
    }
};


function initTableHeader(){
    var tableHeader = document.getElementById('tableHeader');
    teachers.forEach(el => {
        console.log(el.first_name);
        var tableHeaderCell = document.createElement("th");
        tableHeaderCell.innerHTML =
            "<div><img src=\"public/images/icons/circle.svg\" class='icon'></div>\n" +
            "<div>"+el.first_name+"</div>\n" +
            "<div class=\"text--italic\">"+el.last_name+"</div>\n";
        tableHeader.appendChild(tableHeaderCell);
    });
}


function initTableRows() {
    var table = document.getElementById('mainTable');
    table.innerHTML = "";
    //start clean row extra cell from second iteration
    var iteration = 0;
    var iteration2 = 1;

    timings.forEach(el => {

        var tableRow = document.createElement("tr");
        var tableHeaderCell = document.createElement("th");
        tableHeaderCell.setAttribute("scope", "row");
        tableHeaderCell.innerText = el.time;
        tableRow.appendChild(tableHeaderCell);
        var extraCellIndex = [];
        var cellIndex = 1;
        var currentRowID = "rowid_"+iteration;
        var nextRowID = "rowid_"+(iteration+1);
        tableRow.setAttribute("id", currentRowID);
        tableRow.setAttribute("data-id", nextRowID);

        teachers.forEach(teacher => {
            var cell = document.createElement("td");
            classes.forEach(item => {
                let teacherId = teacher.id;

                if(item.staff == teacherId && el.time == item.time){

                    cell.classList.add(item.status);

                    if(item.duration == 1) {
                        cell.setAttribute("rowspan", 2);
                        extraCellIndex.push(cellIndex);

                    }else{
                        cell.setAttribute("rowspan", 1);
                    }

                    cell.innerHTML =
                        "<div data-id="+ item.id +">"+item.time+"<img src=\"public/images/icons/home.svg\" class='icon'></div>\n" +
                        "<div class=\"text--italic\">"+item.id+"</div>\n";

                    cell.setAttribute("data-id", item.id);
                    cell.addEventListener('click',editClass);
                }else{
                }

            });

            tableRow.appendChild(cell);
            cellIndex +=1;


        });


        tableRow.classList.add("js-merge-cell");
        tableRow.setAttribute("data-cells", extraCellIndex);
        //append row to table
        table.appendChild(tableRow);

        if( iteration == iteration2 ){
            iteration2 += 1;
            cleanTableTD();
        }
        iteration +=1;

    });

}

//clean extra cells when rowspan
function cleanTableTD() {
    var i;
    var elementList = document.getElementsByClassName('js-merge-cell');

    for(i = 0; i < elementList.length; i++) {
        var cells = 0;
        var totalRemovable = elementList[i].getAttribute('data-cells');
        var rowId = elementList[i].getAttribute('data-id');
        var rowElem = document.getElementById(rowId);

        if(totalRemovable) {
            var extraCellIndexes = totalRemovable.split(',');
            extraCellIndexes = extraCellIndexes.reverse();
            for (cells = 0; cells < extraCellIndexes.length; cells++) {
                rowElem.deleteCell(extraCellIndexes[cells]);
            }
        }
        elementList[i].classList.remove('js-merge-cell');
    }

}

//add new class
function editClass(e) {
    lightboxReset();
    var classId = "";
    if(e.target.nodeName == 'TD') {
        classId = e.target.dataset.id
    }else{
        classId = e.target.parentNode.dataset.id;
    }

    var classItem = [];
    classes.filter(item => {
        if(new RegExp(classId, "i").test(item.id)){
            classItem.push(item);
        }
    });

    document.getElementById("jsLightBox").classList.add('open');

    document.getElementById('classType').value = classItem[0].type;
    document.getElementById('classTopic').value = classItem[0].topic;
    document.getElementById('classCenter').value = classItem[0].center;
    document.getElementById('classSeats').value = classItem[0].seats;
    document.getElementById('classNotes').value = classItem[0].notes;

    var studentsListElem = document.getElementById('studentsInClass');

    classItem[0].students.forEach(item =>{
        var listItem = document.createElement('li');
        listItem.innerHTML = '<div class="l-cell-x-8 u-text-left u-pl-40 u-p-relative">' +
            '<img src="public/images/icons/user-circle-o.svg" class=\'icon icon-person\'>'+item+'</div>\n' +
            '<div class="l-cell-x-2 u-text-right"> <img src="public/images/icons/trash-o.svg" class=\'icon icon-trash\'> </div>';
        studentsListElem.appendChild(listItem);
    });

}

function searchStudent() {
    let studentId = document.getElementById("searchStudent").value;
    filteredData = [];
    if(studentId.trim() != "") {
        allClassData.filter(item => {
            //if(new RegExp(studentId, "i").test(item.students)){
            if(item.students.includes(studentId.trim())){
                filteredData.push(item);
            }
        });
        if (filteredData.length > 0) {
            classes = filteredData;
            initTableRows();
        }else {
            classes = [];
            initTableRows();
        }
    }else{
        classes = allClassData;
        initTableRows();
    }
}



function lightBoxClose() {
    document.getElementById("jsLightBox").classList.remove('open');
}


function  lightboxReset() {
    document.getElementById('classType').value = "";
    document.getElementById('classTopic').value = "";
    document.getElementById('classCenter').value = "";
    document.getElementById('classSeats').value = "";
    document.getElementById('classNotes').value = "";
    document.getElementById('studentsInClass').innerHTML = "";

}